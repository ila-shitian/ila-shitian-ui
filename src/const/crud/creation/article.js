const DIC = {
  stateFlag: [{
    label: '未审核',
    value: '0'
  }, {
    label: '已通过',
    value: '1'
  }, {
    label: '已拒绝',
    value: '2'
  }]
}

export const tableOption = {
  border: true,
  index: true,
  indexLabel: "序号",
  stripe: true,
  menuAlign: "center",
  align: "center",
  searchMenuSpan: 6,
  addBtn: false,
  delBtn: false,
  column: [
    {
      type: "input",
      label: "主键",
      prop: "id",
      hide: true
    }, {
      type: "input",
      label: "文章标题",
      prop: "title",
      search: true
    }, {
      type: "input",
      label: "类别",
      prop: "categoryId"
    }, {
      type: "input",
      label: "作者",
      prop: "author",
      search: true
    }, {
      type: "input",
      label: "阅读数",
      prop: "readNum"
    }, {
      type: "input",
      label: "点赞数",
      prop: "likeNum"
    }, {
    //   type: "input",
    //   label: "创建人",
    //   prop: "createBy"
    // }, {
      type: "input",
      label: "创建时间",
      prop: "createTime",
      format: 'yyyy-MM-dd HH:mm',
      valueFormat: 'yyyy-MM-dd HH:mm'
    // }, {
    //   type: "input",
    //   label: "修改人",
    //   prop: "updateBy"
    // }, {
    //   type: "input",
    //   label: "修改时间",
    //   prop: "updateTime"
    }, {
      type: "input",
      label: "状态",
      prop: "stateFlag",
      dicData: DIC.stateFlag
    }]
}
