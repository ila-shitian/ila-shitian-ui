const DIC = {
  lockFlag: [{
    label: '有效',
    value: '0'
  }, {
    label: '锁定',
    value: '9'
  }]
}

export const tableOption = {
  border: true,
  index: true,
  indexLabel: "序号",
  stripe: true,
  menuAlign: "center",
  align: "center",
  searchMenuSpan: 6,
  column: [{
    type: "input",
    label: "主键ID",
    prop: "memberId",
    hide: true,
    display: false
  }, {
    type: "input",
    label: "用户名",
    prop: "username"
  }, {
    type: "input",
    label: "真实名字",
    prop: "realName",
    search: true
  }, {
    type: "input",
    label: "手机",
    prop: "phone",
    search: true
  }, {
    type: "input",
    label: "头像",
    prop: "avatar"
  }, {
    type: "radio",
    label: "状态",
    prop: "lockFlag",
    slot: true,
    border: true,
    search: true,
    searchSpan: 3,
    rules: [{
      required: true,
      message: '请选择状态',
      trigger: 'blur'
    }],
    dicData: DIC.lockFlag
  }, {
    type: "input",
    label: "创建时间",
    prop: "createTime",
    display: false
  }]
}
